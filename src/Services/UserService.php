<?php

    namespace App\Services;

    use App\Repository\UtilisateurRepository;
    use App\Repository\LivreRepository;


    class UserService {

        protected $userRepository;
        protected $livreRepository;

        public function __construct(UtilisateurRepository $userRepository, LivreRepository $livreRepository){
            $this->userRepository = $userRepository;
            $this->livreRepository = $livreRepository;
        }

        public function getCurrentUser($user){

            if(!$user) return null;

            $username = $user->getUsername();

            $user = $this->userRepository->findOneBy([
                'email' => $username
            ]);

            return $user;
        }

        public function getCollections($user){
            $books = $this->livreRepository->findBy([
                'owner' => $user->getUsername()
            ]);

            $collections = [];
            foreach($books as $book){
                if(!in_array($book->getCollection(),$collections)){
                    //dd($book->getCollection());
                    $collections[] = $book->getCollection();
                }
            }
            //dd($collections);
            return $collections;
        }
    }