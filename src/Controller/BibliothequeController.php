<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Form\BookType;
use App\Repository\UtilisateurRepository;
use App\Repository\LivreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Services\UserService;

class BibliothequeController extends AbstractController
{

    protected $userService;

    public function __construct(UserService $userService){
        $this->userService = $userService;
    }
    /**
     * Fonction qui permet d'afficher la bibliothéque
     * @Route("/dashboard", name="dashboard")
     * @return void
     */
    public function dashboard(UtilisateurRepository $utilisateurRepository, LivreRepository $livreRepository)
    {
        $user = $this->getUser();
        $utlisateur = $utilisateurRepository->findOneBy([
            'email' => $user->getUsername()
        ]);

        $books = $livreRepository->getLastInsertedBook($user->getUsername());
        $landedBooks = $livreRepository->findBy([
            'owner' => $user->getUsername(),
            'disponible' => false
        ]);
        
        $nombreLivres = count($livreRepository->findBy([
            'owner' => $user->getUsername()
        ]), COUNT_RECURSIVE);

        $nombreLivresPretes = count($landedBooks, COUNT_RECURSIVE);

        return $this->render('dashboard/dashboard.html', [
            'user' => $utlisateur,
            'nombreLivres' => $nombreLivres,
            'books' => $books,
            'nombreLivresPretes' => $nombreLivresPretes
        ]);
    }

    /**
     * Fonction qui permet d'ajouter un livre à notre bibliothéque
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/addBook", name="addBook")
     * @Route("/editBook/{id}", name="editBook")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return void
     */
    public function addBook(Livre $book = null, Request $request, EntityManagerInterface $manager){

        $editMode = true;

        if(!$book){
            $book = new Livre();
            $editMode = false;
        }

        $utilisateur = $this->userService->getCurrentUser($this->getUser());

        
        $form = $this->createForm(BookType::class,$book);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            
            $book->setOwner($this->getUser()->getUsername());
            if(!$editMode){
                $book->setDisponible(true);
            }
            $manager->persist($book);
            $manager->flush();

            return $this->redirectToRoute('books');
        }

        return $this->render('dashboard/ajout_livre.html',[
            'user' => $utilisateur,
            'form' => $form->createView(),
            'editMode' => $editMode
        ]);
    }

     /**
     * Fonction qui permet de lister les livres de  notre bibliothéque
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/books", name="books")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return void
     */
    public function showBookList(LivreRepository $bookRepository){
        $user = $this->userService->getCurrentUser($this->getUser());

        $books = $bookRepository->findBy([
            'owner' => $user->getEmail()
        ]);

        $nombreLivres = count($books, COUNT_RECURSIVE);

        return $this->render('dashboard/liste_livre.html',[
            'user' => $user,
            'books' => $books,
            'nombreLivres' => $nombreLivres
        ]);
    }

    /**
     * Fonction qui permet de lister les livres de  notre bibliothéque
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/listlandedbooks", name="listlandedbooks")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return void
     */
    public function showListLandedBook(LivreRepository $bookRepository){
        $user = $this->userService->getCurrentUser($this->getUser());

        $books = $bookRepository->findBy([
            'owner' => $user->getEmail(),
            'disponible' => false
        ]);

        $nombreLivres = count($books, COUNT_RECURSIVE);

        return $this->render('dashboard/liste_livre_prete.html',[
            'user' => $user,
            'books' => $books,
            'nombreLivres' => $nombreLivres
        ]);
    }

      /**
     * Fonction qui permet de supprimer un livre de notre bibliothéque
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/deleteBook/{id}", name="deleteBook")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return void
     */
    public function deleteBook(Livre $book, EntityManagerInterface $manager){
        $manager->remove($book);
        $manager->flush();

        return $this->redirect($this->generateUrl('books'));
    }

      /**
     * Undocumented function
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/landBook/{id}", name="landBook")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return void
     */
    public function landBook(Livre $book, EntityManagerInterface $manager, Request $request){

        $prenom = $request->get('prenom');
        $nom = $request->get('nom');
        $datePret = $request->get('datePret');
        
        $book->setNomBeneficiaire($nom);
        $book->setPrenomBeneficiaire($prenom);
        $book->setLandingDate( $datePret);
        $book->setDisponible(false);
    

        $manager->persist($book);
        $manager->flush();


        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

     /**
     * Undocumented function
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/livreRendu/{id}", name="livreRendu")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return void
     */
    public function livreRendu(Livre $book, EntityManagerInterface $manager, Request $request){

        $book->setNomBeneficiaire(NULL);
        $book->setPrenomBeneficiaire(NULL);
        $book->setLandingDate(NULL);
        $book->setDisponible(true);
    

        $manager->persist($book);
        $manager->flush();


        return $this->redirect($request->server->get('HTTP_REFERER'));
    }
}
