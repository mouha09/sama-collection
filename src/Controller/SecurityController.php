<?php

namespace App\Controller;

use App\Entity\Service;
use App\Entity\User;
use App\Entity\Utilisateur;
use App\Form\RegisterType;
use App\Repository\ServiceRepository;
use App\Form\ServiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UtilisateurRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use App\Services\Commande\CommandeService;
use Cocur\Slugify\Slugify;
use Cocur\Slugify\SlugifyInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\String\Slugger\SluggerInterface;

class SecurityController extends AbstractController
{
    /**
     * Fonction qui permet d'authentifier les utilisateurs
     * @Route("/sign-in", name="security_login")
     * @param UserRepository $userRepo
     * @return void
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {

        $user = $this->getUser();

        if ($user) {
            return $this->redirectToRoute('accueil');
        }


        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();
        $user = new Utilisateur();
        $form = $this->createForm(RegisterType::class, $user);
        return $this->render('frontside/login.html', [
            'form' => $form->createView(),
            'error'  => $error,
            'lastUsername' => $lastUsername,
            'isForLogin' => true,
        ]);
    }

    /**
     * Methode qui permet d'inscrire un nouvel utilisateur
     * @Route("/sign-up", name="security_register")
     * @return void
     */
    public function register(Request $request, UserRepository $userRepo, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {

        $user = $this->getUser();

        if ($user) {
            return $this->redirectToRoute('accueil');
        }


        $user = new Utilisateur();

        $form = $this->createForm(RegisterType::class, $user, [
            'action' => $this->generateUrl('security_register'),
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $utilisateur = new User();
            $hash = $encoder->encodePassword($utilisateur, $user->getPassword());
            $user->setPassword($hash);

            $utilisateur->setUsername($user->getEmail());

            $utilisateur->setPassword($user->getPassword());

            $manager->persist($user);
            $manager->persist($utilisateur);

            $manager->flush();

            $this->addFlash('register_success', 'Votre inscription s\'est deroulée avec succes, vous pouvez à présent vous connecter avec votre compte');
        }
        return $this->render('frontside/login.html', [
            'form' => $form->createView(),
            'isForLogin' => false,
        ]);
    }

    /**
     * Fonction de deconnexion
     * @Route("/sign-out", name="security_logout")
     * @return void
     */
    public function logout()
    {
    }
}
