<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\LivreRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Services\UserService;

class IndexController extends AbstractController
{
    protected $userService;
    public function __construct(UserService $userService){
        $this->userService = $userService;
    }

    /**
     * @Route("/accueil", name="accueil")
     */
    public function index(LivreRepository $livreRepository)
    {
        $books = [];
        $collections = [];
        if($this->getUser()){
            $books = $livreRepository->getLastInsertedBook($this->getUser()->getUsername());
            $collections = $this->userService->getCollections($this->getUser());
        }


        return $this->render('frontside/index.html', [
           'books' => $books,
           'collections' => $collections
        ]);
    }

    /**
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/myBooks", name="myBooks")
     */
    public function myBooks(LivreRepository $livreRepository){
        $books = $livreRepository->findBy([
            'owner' => $this->getUser()->getUsername()
        ]);
        
        if($this->getUser()){
            $collections = $this->userService->getCollections($this->getUser());
        }

        return $this->render('frontside/mybooks.html',[
            'books' => $books,
            'collections' => $collections,
            'collection' => NULL
        ]);
    }

    /**
     * Require ROLE_USER
     * @IsGranted("ROLE_USER")
     * @Route("/myBooks/{collection}", name="myBooksByCollection")
     */
    public function myBooksByCollection(LivreRepository $livreRepository, $collection){
        $books = $livreRepository->findBy([
            'owner' => $this->getUser()->getUsername(),
            'collection' => $collection
        ]);
        
        if($this->getUser()){
            $collections = $this->userService->getCollections($this->getUser());
        }

        return $this->render('frontside/mybooks.html',[
            'books' => $books,
            'collections' => $collections,
            'collection' => $collection
        ]);
    }

     /**
     * @Route("/contact", name="contact")
     */
    public function contact(){
        $collections = [];
        if($this->getUser()){
            $collections = $this->userService->getCollections($this->getUser());
        }

        //dd($collections);

        return $this->render('frontside/contact.html', [
  
           'collections' => $collections
        ]);
    }
    
}
