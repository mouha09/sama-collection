<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201115223917 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE livre ADD nom_beneficiaire VARCHAR(255) DEFAULT NULL, ADD prenom_beneficiaire VARCHAR(255) DEFAULT NULL, ADD date_pret DATE DEFAULT NULL, CHANGE disponible disponible TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE livre DROP nom_beneficiaire, DROP prenom_beneficiaire, DROP date_pret, CHANGE disponible disponible TINYINT(1) DEFAULT \'1\'');
    }
}
