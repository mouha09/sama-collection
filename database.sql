-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 17, 2020 at 05:52 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `sama-collection`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20201115161920', '2020-11-15 16:23:02', 115),
('DoctrineMigrations\\Version20201115163221', '2020-11-15 16:32:42', 139),
('DoctrineMigrations\\Version20201115163435', '2020-11-15 17:27:23', 117),
('DoctrineMigrations\\Version20201115172709', '2020-11-15 17:27:23', 30),
('DoctrineMigrations\\Version20201115180509', '2020-11-15 18:05:58', 142),
('DoctrineMigrations\\Version20201115193241', '2020-11-15 19:32:58', 108),
('DoctrineMigrations\\Version20201115194142', '2020-11-15 19:42:51', 85),
('DoctrineMigrations\\Version20201115194639', '2020-11-15 20:03:21', 143),
('DoctrineMigrations\\Version20201115200622', '2020-11-15 20:35:47', 129),
('DoctrineMigrations\\Version20201115204359', '2020-11-15 20:44:20', 120),
('DoctrineMigrations\\Version20201115223917', '2020-11-15 22:39:34', 145),
('DoctrineMigrations\\Version20201116013608', '2020-11-16 01:36:26', 137);

-- --------------------------------------------------------

--
-- Table structure for table `livre`
--

CREATE TABLE `livre` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auteur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_edition` date DEFAULT NULL,
  `collection` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `synopsis` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upadated_at` datetime DEFAULT NULL,
  `disponible` tinyint(1) DEFAULT NULL,
  `nom_beneficiaire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom_beneficiaire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_pret` date DEFAULT NULL,
  `landing_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `livre`
--

INSERT INTO `livre` (`id`, `titre`, `auteur`, `date_edition`, `collection`, `synopsis`, `image`, `owner`, `upadated_at`, `disponible`, `nom_beneficiaire`, `prenom_beneficiaire`, `date_pret`, `landing_date`) VALUES
(1, 'Python for data analysis', 'Mouhmmad Sylla', '2020-02-02', 'Data Science', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Temporibus, voluptatibus!', 'python-5fb1914c38175621754441.jpg', 'mouhammad.sylla@gmail.com', NULL, 0, 'Tall', 'Salimata', NULL, '2020-11-27'),
(3, 'Senegal, son developpement de par une politique informatisée', 'Mouhmmad Sylla', '2022-04-09', 'Politique', NULL, 'senghor-5fb1e0bc378f5185798312.jpg', 'mouhammad.sylla@gmail.com', NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `roles`) VALUES
(2, 'mouhammad.sylla@gmail.com', '$2y$13$0.wAeuhxVdknV.I.lhBnH.G0xwiTDbJxi.VT3EM.gieaQOqUyL7eK', NULL),
(4, 'fatoudiop@gmail.com', '$2y$13$HZiKkRhbtnTvnClaw3MlNejO6RabzKPBawxCi69dPkka0u10WAlAC', '[]'),
(5, 'fatoudiop2@gmail.com', '$2y$13$FK8MGJopcrvbeU69H9OLf.5MgsJUR6PgX8KdQvmOAOsWl76jCS3RK', '[]'),
(6, 'fatoudiop3@gmail.com', '$2y$13$tf0oELuWD6yFthlGGLdz5u2EwMANMYYi2.L7aroRMkYJqHqvhM8em', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numtel` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `prenom`, `nom`, `numtel`, `photo`, `email`, `password`) VALUES
(1, 'Mouhammad', 'Sylla', 778285415, NULL, 'mouhammad.sylla@gmail.com', '$2y$13$0.wAeuhxVdknV.I.lhBnH.G0xwiTDbJxi.VT3EM.gieaQOqUyL7eK'),
(2, 'Fatou', 'Diop', 765432123, NULL, 'fatoudiop@gmail.com', '$2y$13$HZiKkRhbtnTvnClaw3MlNejO6RabzKPBawxCi69dPkka0u10WAlAC'),
(3, 'Fatou', 'Diop', 765432122, NULL, 'fatoudiop2@gmail.com', '$2y$13$FK8MGJopcrvbeU69H9OLf.5MgsJUR6PgX8KdQvmOAOsWl76jCS3RK'),
(4, 'Fatou', 'Diop', 765432121, NULL, 'fatoudiop3@gmail.com', '$2y$13$tf0oELuWD6yFthlGGLdz5u2EwMANMYYi2.L7aroRMkYJqHqvhM8em');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `livre`
--
ALTER TABLE `livre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `livre`
--
ALTER TABLE `livre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
